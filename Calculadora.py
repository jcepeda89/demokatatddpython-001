class Calculadora:
    def sumar(self, cadena):
        if cadena == "":
            return 0
        elif "," in cadena or "&" in cadena or ":" in cadena:
            cadena = cadena.replace("&", ",")
            cadena = cadena.replace(":", ",")
            numeros = cadena.split(",")
            suma = 0
            for numero in numeros:
                suma = suma + int(numero)
            return suma
        else:
            return int(cadena)
